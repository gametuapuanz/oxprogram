
import java.util.Scanner;

public class GameXO {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);

		printwelcome();
		showTable();
		showTurn(kb);
	
for (int count = 0; count < 8;) {
			count = showTurn(kb, board, count);
			if (checkWin() == false) {
				break;
			}
			if (checkDraw() == false) {
				break;
			}
		}

	}

	public static void showTurn(Scanner kb) {
		int row;
		int colum;
		System.out.println("X Turn.");
		System.out.print("please input Row Col : ");
		row = kb.nextInt();
		colum = kb.nextInt();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				board[row][colum] = " X";
				System.out.print(board[i][j]);
			}
			System.out.println();
		}
	}


	public static void showTable() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(board[i][j]);
			}
			System.out.println();
		}
	}


	public static int showTurn(Scanner kb, String[][] board, int count) {
		int row;
		int colum;
		if (count % 2 == 1) {
			System.out.println("X Turn.");
			System.out.print("please input Row Col : ");
			row = kb.nextInt();
			colum = kb.nextInt();

			if (board[row][colum] == " -") {
				board[row][colum] = " X";
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						System.out.print(board[i][j]);
					}
					System.out.println();
				}
				count += 1;

			} else if (board[row][colum] == " O" || board[row][colum] == " X") {
				count += 0;
			}

		}

		else if (count % 2 == 0) {
			System.out.println("O Turn.");
			System.out.print("please input Row Col : ");
			row = kb.nextInt();
			colum = kb.nextInt();

			if (board[row][colum] == " -") {
				board[row][colum] = " O";
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						System.out.print(board[i][j]);
					}
					System.out.println();
				}
				count += 1;

			} else if (board[row][colum] == " O" || board[row][colum] == " X") {
				count += 0;

			}
		}
		return count;
	}

	static boolean checkDraw() {
		if (board[1][1] != " -" && board[1][2] != " -" && board[1][3] != " -" && board[2][1] != " -"
				&& board[2][2] != " -" && board[2][3] != " -" && board[3][1] != " -" && board[3][2] != " -"
				&& board[3][3] != " -") {
			System.out.println("Draw");
			System.out.println("Bye Bye!");
			return false;
		}else
			return true;
		
	}
	static boolean checkWin() {
		if (board[1][1] == " X" && board[2][1] == " X" && board[3][1] == " X"
				|| board[1][2] == " X" && board[2][2] == " X" && board[2][3] == " X"
				|| board[3][1] == " X" && board[3][2] == " X" && board[3][3] == " X"
				|| board[1][1] == " X" && board[1][2] == " X" && board[1][3] == " X"
				|| board[2][1] == " X" && board[2][2] == " X" && board[2][3] == " X"
				|| board[1][3] == " X" && board[2][3] == " X" && board[3][3] == " X"
				|| board[1][1] == " X" && board[2][2] == " X" && board[3][3] == " X"
				|| board[1][3] == " X" && board[2][2] == " X" && board[3][1] == " X") {
			System.out.println("X is Winner");
			return false;
		}
		if (board[1][1] == " O" && board[2][1] == " O" && board[3][1] == " O"
				|| board[1][2] == " O" && board[2][2] == " O" && board[3][2] == " O"
				|| board[3][1] == " O" && board[3][2] == " O" && board[3][3] == " O"
				|| board[1][1] == " O" && board[1][2] == " O" && board[1][3] == " O"
				|| board[2][1] == " O" && board[2][2] == " O" && board[2][3] == " O"
				|| board[1][3] == " O" && board[2][3] == " O" && board[3][3] == " O"
				|| board[1][1] == " O" && board[2][2] == " O" && board[3][3] == " O"
				|| board[1][3] == " O" && board[2][2] == " O" && board[3][1] == " O") {
			System.out.println("O is Winner");
			return false;
		}else
			return true;
		
	}

	static String[][] board = { { " ", " 1", " 2", " 3" }, { "1", " -", " -", " -" }, { "2", " -", " -", " -" },
			{ "3", " -", " -", " -" } };

	private static void printwelcome() {
		System.out.println("Welcome to OX game.");
		System.out.println();
	}
}
